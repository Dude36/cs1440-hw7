#include <iostream>
#include <map>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>

const std::string URL_REGEX = "<a href=\"(http://[^ #?]+)\">";
// const std::string URL_REGEX = "<[ \t\n]*a[ \t\n]+.*href=\"(http://[^ #?]*)\"[ \t\n]*>";
// const std::string TEST_REGEX = "<a .*?href="((?:(https?://)([^/"#]+)|/)([^"]*))".*?>";

void recurseCrawl(std::map<std::string, int>&, std::string, int, int);
bool inDict(std::map<std::string, int>, std::string);
std::string spacing(int);

int main(int argc, char** argv) {
    std::string there = "http://cs.usu.edu";
    int maxDepth = 3;

    if (argc > 1)
        there = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);

    std::cout << "Recursively crawling the web to a depth of " << maxDepth << " links beginning from " << there << std::endl;
    
    std::map<std::string, int> dict;
    recurseCrawl(dict, there, maxDepth, 0);
    
    std::cout << "============= Full Dictionary =================" << std::endl;
    for (auto item : dict) {
        std::cout << spacing(item.second) << item.first << std::endl;
    }

    return 0;
}

void recurseCrawl(std::map<std::string, int> &dictionary, std::string searchURL, int maxDepth, int currentDepth) {
    if (inDict(dictionary, searchURL)) {
        // std::cout << "Already in the dictionary: " << searchURL  << "\tDepth: " << dictionary[searchURL] << std::endl;
        return;
    }
    if (currentDepth >= maxDepth)
        return;
    
    // Place URL in the Dictionary because we can still recurse, and it's unique
    dictionary[searchURL] = currentDepth;
    try {
        // Setup Curl
        curlpp::Cleanup cleaner;
        
        // cURL the website
        curlpp::Easy request;
        request.setOpt(curlpp::options::Url(searchURL));
        request.setOpt(curlpp::options::FollowLocation(true));
        
        std::ostringstream os;
        os << request << std::endl;
        
        // Get the a tags
        std::regex re(URL_REGEX);
    	std::smatch m;
    	std::string s = os.str();
    	
        // Recurse on self wil all the a tags
        while(std::regex_search(s, m, re)) {
            // Verify that the string isn't in the dictionary
            if (inDict(dictionary, m[1].str())) {
                // Already in the list
    		    // std::cout << "||\tAlready in the dictionary: " << searchURL  << "\t| Depth: " << dictionary[searchURL] << std::endl;
            } else {
                // Print & Recurse
                std::cout << spacing(currentDepth) << m[1].str() << std::endl;
                // Parse string with the whole URL !!Not Used because the REGEX given doesn't work!!
                // if (m[2].str().size() == 0) {
                //     std::cout << "Match Groups:" << std::endl << "\t0: " << m[0].str() << std::endl << "\t1: " << m[1].str() << std::endl
                //         << "\t2: " << m[2].str() << std::endl << "\t3: " << m[3].str() << std::endl;
                //     recurseCrawl(dictionary, searchURL + m[1].strt(), maxDepth, currentDepth+1);
                // } else {
                //     recurseCrawl(dictionary, m[1].str() + m[2].str() + m[3].str() + m[4].str(), maxDepth, currentDepth+1);
                // }
                recurseCrawl(dictionary, m[1].str(), maxDepth, currentDepth+1);
            }
            
            // Get the rest of the string
    		s = m.suffix().str();
        }
    } catch ( curlpp::LogicError & e ) {
        std::cout << e.what() << std::endl;
    } catch ( curlpp::RuntimeError & e ) {
        std::cout << e.what() << std::endl;
    } 
}

bool inDict(std::map<std::string, int> m, std::string k) {
    if (m.count(k) > 0) {
        return true;
    }
    return false;
}

std::string spacing(int num) {
    std::string s = "";
    for (int i = 0; i < num; i++) {
        s += "\t";
    }
    return s;
}
